Source: ocsinventory-server
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Yadd <yadd@debian.org>,
           Cyrille Bollu <cyrille@bollu.be>
Section: web
#Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               apache2-dev
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/ocsinventory-server
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/ocsinventory-server.git
Homepage: https://www.ocsinventory-ng.org/
Rules-Requires-Root: binary-targets

Package: ocsinventory-server
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libapache-dbi-perl,
         libapache2-mod-perl2,
         libarchive-zip-perl,
         libdbd-mysql-perl,
         libdbi-perl,
         libnet-ip-perl,
         libswitch-perl,
         libxml-simple-perl
Recommends: ${misc:Recommends},
            ocsinventory-reports
Suggests: default-mysql-server | virtual-mysql-server,
          nmap,
          ocsinventory-agent,
          samba-common
Description: Hardware and software inventory tool (Communication Server)
 Open Computer and Software Inventory Next Generation is an application
 designed to help a network or system administrator keep track of the
 computers configuration and software that are installed on the network.
 .
 Information about Hardware and Operating System are collected.
 OCS Inventory is also able to detect all active devices on your network,
 such as switch, router, network printer and unattended devices.
 It also allows deploying software, commands or files on client computers.
 .
 This package contains the 'Communication Server' part.
 Note that this is the XML-RPC/SOAP server, not the web interface (which is
 provided in the ocsinventory-reports package).

Package: ocsinventory-reports
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         default-mysql-client | virtual-mysql-client,
         ieee-data,
         javascript-common,
         libapache2-mod-php | php-cgi | php-fpm,
         libjs-bootstrap,
         libjs-jquery,
         libjs-jquery-datatables,
         libjs-jquery-file-upload,
         libjs-jquery-migrate-1,
         libjs-jquery-ui,
         libjs-select2.js,
         libphp-phpmailer,
         php,
         php-cas,
         php-cli,
         php-gd,
         php-imap,
         php-ldap,
         php-mbstring,
         php-mysql,
         php-pclzip,
         php-soap,
         php-xml,
         php-zip,
         phpqrcode
Recommends: ${misc:Recommends},
            libdbd-mysql-perl,
            libdbi-perl,
            libnet-ip-perl,
            libxml-simple-perl,
            nmap,
            ocsinventory-server,
            samba-common
Description: Hardware and software inventory tool (Administration Console)
 Open Computer and Software Inventory Next Generation is an application
 designed to help a network or system administrator keep track of the
 computers configuration and software that are installed on the network.
 .
 Information about Hardware and Operating System are collected.
 OCS Inventory is also able to detect all active devices on your network,
 such as switch, router, network printer and unattended devices.
 It also allows deploying software, commands or files on client computers.
 .
 This package contains the 'Administration Console' part.
